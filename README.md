# Constitution-template introduction

These documents describe a framework for producing social games. It incorporates the Debian constitution and was originally intended for adapting that document to other organizations, like technology co-ops or other projects for peer-production of common value.
These documents have been prepared in the hope that they may be useful not only as a guide to worthwhile organizational "games" but also as a guide to logical validation of properties of social games.

# Inspirations & Influences

## _The Abolition of Work_ by Bob Black.

A copy is available here: https://theanarchistlibrary.org/library/bob-black-the-abolition-of-work/

## Delib's communications training project

https://salsa.debian.org/Delib/communications_training


# What's a social game?

A social game is played by members of a society.
A game is a list of rules. A rule is a statement. Statements are about game entities and operations on them. A rule is a game entity, and all game entities are defined by rules. Some rules can be implicit during normal play, but explicated in logical analysis.

## Example: Musical Chairs

Musical Chairs is an "elimination" game that reduces the set of players to a single winner, the last person sitting in a chair.

## Example: Bucket Brigade

A bucket brigade is a less-obvious example of a social game. It's a game for moving water.

## Example: Robots' Rules of Order

Robert's Rules of Order is a decision-making metagame.

## Example: The Debian Project

The Debian Project is a metagame for peer-production of a coherent collection of computer software. It subsumes a specific voting game and some unspecified consensus games.

# Copying

## Files

If you add any files to the project, please list them here.

### The Debian Constitution
- debian-constitution.html (copied from https://debian.org/devel/constitution)
- debian-constitution.org (modified version of above)

The Debian Constitution's copyright belongs to Software in the Public Interest, per https://www.debian.org/license

Since these documents are copies and derivative works of the Debian Constitution, their copyright also belongs to SPI, licensable under GNU GPL version 2 or later.

### constitution-template.org

A fill-in-the-blank Org-mode file you can use to generate a constitution document in the export formats Org supports. HTML is supported, others should also work.

The Constitution Template copyright belongs to $lol_idk
Files generated from the Constitution Template are copyright $your_organization

### README.md

This file.
This file's copyright belongs to $lol_idk

### NOTES.org

Pastes and other blurbs to be copied or transcluded into other documents.
Copyright is the same as that of constitution-template